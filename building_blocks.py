import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.nn.init as init
from spectral_normalization import SpectralNorm

from collections import OrderedDict

class _DenseLayer(nn.Sequential):
    def __init__(self, num_input_features, growth_rate, bn, bn_size, drop_rate):
        super(_DenseLayer, self).__init__()
        if bn:
            self.add_module('norm_1', nn.BatchNorm2d(num_input_features)),
        self.add_module('relu_1', nn.ReLU(inplace=True)),
        self.add_module('conv_1', nn.Conv2d(num_input_features, bn_size *
                        growth_rate, kernel_size=1, stride=1, bias=not bn)),
        if bn:
            self.add_module('norm_2', nn.BatchNorm2d(bn_size * growth_rate)),
        self.add_module('relu_2', nn.ReLU(inplace=True)),
        self.add_module('conv_2', nn.Conv2d(bn_size * growth_rate, growth_rate,
                        kernel_size=3, stride=1, padding=1, bias=not bn)),
        self.drop_rate = drop_rate

    def forward(self, x):
        new_features = super(_DenseLayer, self).forward(x)
        if self.drop_rate > 0:
            new_features = F.dropout(new_features, p=self.drop_rate, training=self.training)
        return torch.cat([x, new_features], 1)

class _DenseBlock(nn.Sequential):
    def __init__(self, num_layers, num_input_features, bn, bn_size, growth_rate, drop_rate):
        super(_DenseBlock, self).__init__()
        for i in range(num_layers):
            layer = _DenseLayer(num_input_features + i * growth_rate, growth_rate, bn, bn_size, drop_rate)
            self.add_module('denselayer%d' % (i + 1), layer)

class _Transition(nn.Sequential):
    def __init__(self, num_input_features, num_output_features, bn):
        super(_Transition, self).__init__()
        if bn:
            self.add_module('norm', nn.BatchNorm2d(num_input_features))
        self.add_module('relu', nn.ReLU(inplace=True))
        self.add_module('conv', nn.Conv2d(num_input_features, num_output_features,
                                          kernel_size=1, stride=1, bias=not bn))
#         self.add_module('pool', nn.AvgPool2d(kernel_size=2, stride=2))

class _NormalizedConv2d(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride=1, padding=0, dilation=1, bias=False):
        super(_NormalizedConv2d, self).__init__()
        self.in_channels = in_channels
        self.conv = nn.Conv2d(in_channels, out_channels, kernel_size, stride=stride,
                              padding=padding, dilation=dilation, bias=False)
        # self.conv.weight.data = torch.randn_like(self.conv.weight.data) - 0.5
        # self.norm_conv = torch.nn.utils.weight_norm(self.conv)
        self.do_bias = bias
        if bias:
            self.bias = nn.Parameter(torch.zeros((out_channels, 1, 1)), requires_grad=True)
        
    def forward(self, x):
        certainty = (x != 0).float()
        conv_out = self.conv(x)
        cert_out = self.conv(certainty)
        normed = torch.where(cert_out!=0, conv_out/cert_out, torch.zeros_like(conv_out))
        if self.do_bias:
            normed = normed + self.bias
        return normed
    
class _GatedConv2d(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride=1, padding=0, dilation=1, bias=False):
        super(_GatedConv2d, self).__init__()
        self.in_channels = in_channels
        self.conv = nn.Conv2d(in_channels, out_channels, kernel_size, stride=stride,
                              padding=padding, dilation=dilation, bias=False)
        self.soft_gating = nn.Conv2d(in_channels, out_channels, kernel_size, stride=stride,
                                     padding=padding, dilation=dilation, bias=False)
        
        # self.conv.weight.data = torch.randn_like(self.conv.weight.data) - 0.5
        # self.norm_conv = torch.nn.utils.weight_norm(self.conv)
        self.bias = nn.Parameter(torch.zeros((out_channels, 1, 1)), requires_grad=True)
        
    def forward(self, x):
        feature = F.leaky_relu(self.conv(x) + self.bias, 0.2)
        gating = F.sigmoid(self.soft_gating(x))
        
        return feature * gating
    
class _NonLocalBlock2d(nn.Module):
    def __init__(self, in_channels, bn=True):
        super(_NonLocalBlock2d, self).__init__()
        
        self.in_channels = in_channels
        self.h_channels = self.in_channels // 2
        
        self.th = nn.Conv2d(in_channels, self.h_channels, 1)
        self.phi = nn.Conv2d(in_channels, self.h_channels, 1)
        self.g = nn.Conv2d(in_channels, self.h_channels, 1)
        
        self.W = _ConvNormReLU(self.h_channels, in_channels, 1, 1, 0, 1, norm='bn' if bn else 'no')
        if bn:
            self.W = nn.Sequential(self.W, nn.BatchNorm2d(in_channels))
        
    def forward(self, x):
        bs = x.size(0)
        
        th_out = self.th(x).view(bs, self.h_channels, -1).permute(0, 2, 1)
        phi_out = self.phi(x).view(bs, self.h_channels, -1)
        
        f = F.softmax(torch.matmul(th_out, phi_out), dim=-1)
        g_out = self.g(x).view(bs, self.h_channels, -1).permute(0, 2, 1)
        y = torch.matmul(f, g_out).permute(0,2,1).contiguous().view(bs, self.h_channels, x.size(2), x.size(3))
        
        return self.W(y) + x
        
# class _PartialConv2d(nn.Module):
#     def __init__(self, in_channels, out_channels, kernel_size, stride=1, padding=0, dilation=1, bias=False):
#         super(_PartialConv2d, self).__init__()
#         self.in_channels = in_channels
#         self.conv = nn.Conv2d(in_channels, out_channels, kernel_size, stride=stride,
#                               padding=padding, dilation=dilation, bias=False)
#         self.mask_conv = nn.Conv2d(in_channels, out_channels, kernel_size, stride=stride,
#                               padding=padding, dilation=dilation, bias=False)
#         self.mask_conv.weight.data = torch.ones_like(self.conv.weight.data)
#         for p in self.mask_conv.parameters():
#             p.requires_grad = False
#         # self.conv.weight.data = torch.randn_like(self.conv.weight.data) - 0.5
#         # self.norm_conv = torch.nn.utils.weight_norm(self.conv)
#         self.do_bias = bias
#         if bias:
#             self.bias = nn.Parameter(torch.zeros((out_channels, 1, 1)), requires_grad=True)
#         
#     def forward(self, x):
#         certainty = (x != 0).float()
#         conv_out = self.conv(x)
#         cert_out = self.conv(certainty)
#         normed = torch.where(cert_out!=0, conv_out/cert_out, torch.zeros_like(conv_out))
#         if self.do_bias:
#             normed = normed + self.bias
#         return normed

class _DilatedBlock(nn.Module):
    def __init__(self, num_features, dilation, bn):
        super(_DilatedBlock, self).__init__()
        self.add_module('conv', nn.Conv2d(num_features, num_features, kernel_size=3, dilation=dilation, padding=dilation, bias=not bn))
        
        if bn:
            self.add_module('bn', nn.BatchNorm2d(num_features))
        self.add_module('relu', nn.LeakyReLU(0.2))
        
    def forward(self, input):
        output = self.main(input)
        return output

    
class _ConvNormReLU(nn.Sequential):
    def __init__(self, in_channels, out_channels, kernel_size, stride, padding, dilation, norm='bn', lrelu=False, part_conv=False, fs=None, transposed=False):
        super(_ConvNormReLU, self).__init__()
        
        if not transposed:
            if part_conv:
                conv_f = _GatedConv2d
            else:
                conv_f = nn.Conv2d
            conv = conv_f(
                    in_channels=in_channels,
                    out_channels=out_channels,
                    kernel_size=kernel_size,
                    stride=stride,
                    padding=padding,
                    dilation=dilation,
                    bias = norm!='bn')
            if lrelu:
                init.kaiming_normal_(conv.conv.weight if part_conv else conv.weight, a=0.2, nonlinearity='leaky_relu')
            else:
                init.kaiming_normal_(conv.conv.weight if part_conv else conv.weight, nonlinearity='relu')
        else:
            conv = nn.ConvTranspose2d(
                in_ch, out_ch, kernel_size, stride=2, padding=1, dilation=1, bias=norm!='bn')
            conv.weight.data = utils.bilinear_kernel(4, in_ch, in_ch).float()
        
        norm_layer = None
        if norm == 'bn':
            norm_layer = nn.BatchNorm2d(num_features=out_channels, eps=1e-5, momentum=0.99)
        elif norm == 'in':
            norm_layer = nn.InstanceNorm2d(num_features=out_channels, eps=1e-5, momentum=0.99)
        elif norm == 'ln':
            if fs is None:
                raise Exception("You need to specify feature map spatial size for layer narmalization")
            norm_layer = nn.LayerNorm((out_channels, fs, fs))
        elif norm == 'sn':
            conv = SpectralNorm(conv)

        self.add_module('conv', conv)
        if norm_layer is not None:
            self.add_module('norm', norm_layer)
        self.add_module('relu', nn.LeakyReLU(0.2) if lrelu else nn.ReLU())

    def forward(self, x):
        return super(_ConvNormReLU, self).forward(x)
    
    
class _UpsampleBlock(nn.Module):
    def __init__(self, in_ch, out_ch, kernel_size, norm='bn'):
        super(_UpsampleBlock, self).__init__()
        self.block = nn.Sequential()
        self.block.add_module('up', nn.Upsample(scale_factor=2))
        self.block.add_module('conv_bn_relu_1', _ConvNormReLU(
            in_ch, out_ch, kernel_size, 1, 1, 1, norm=norm))
        self.block.add_module('conv_bn_relu_2', _ConvNormReLU(
            out_ch, out_ch, kernel_size, 1, 1, 1, norm=norm))
        
    def forward(self, x):
        return self.block(x)


class _ASPPModule(nn.Module):
    """Atrous Spatial Pyramid Pooling with image pool"""

    def __init__(self, in_channels, out_channels, pyramids, bn):
        super(_ASPPModule, self).__init__()
        self.stages = nn.Module()
        self.pyramids = pyramids
        self.depthwise = _ConvBatchNormReLU(in_channels, out_channels, 1, 1, 0, 1, bn=bn)
        for i, (dilation, padding) in enumerate(zip(pyramids, pyramids)):
            self.stages.add_module(
                'c{}'.format(i + 1),
                _ConvBatchNormReLU(in_channels, out_channels, 3, 1, 0, dilation, bn=bn), #padding, dilation),
            )

    def forward(self, x):
#         h = self.imagepool(x)
#         h = [F.upsample(h, size=x.shape[2:], mode='bilinear')]
        h = [self.depthwise(x)]
        for i, stage in enumerate(self.stages.children()):
            padded = F.pad(x, (self.pyramids[i], self.pyramids[i], self.pyramids[i], self.pyramids[i]), mode='reflect')
            h += [stage(padded)]
        h = torch.cat(h, dim=1)
        return h

    
class _Flatten(nn.Module):
    def __init__(self):
        super(_Flatten, self).__init__()

    def forward(self, x):
        return x.view(x.size(0), -1)