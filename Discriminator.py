import torch
import torch.nn as nn
import math
import numpy as np
import building_blocks
                
                
class GrowableDiscriminator(nn.Module):
    def __init__(self, input_channels=4, input_size=16, final_nf=384, norm='bn'):
        super(GrowableDiscriminator, self).__init__()
        self.cur_nf = final_nf
        self.init_res = self.cur_res = input_size
        self.input_channels = input_channels
        self.norm = norm
        
        self.from_rgb = building_blocks._ConvNormReLU(
            self.input_channels, self.cur_nf, 1, 1, 0, 1,
            norm=norm, lrelu=True, fs=input_size if norm=='ln' else None)
        self.model = nn.Sequential()
        for i in range(int(np.log2(input_size//4))):
            fs = input_size//2**i
            self.model.add_module("conv_{}x{}".format(fs,fs), self.__intermediate_block(
                final_nf, double=False, norm=norm, fs=fs if norm=='ln' else None))
        self.model.add_module("final_conv", building_blocks._ConvNormReLU(
            final_nf, final_nf, 4, 1, 0, 1, norm='no', lrelu=True, fs=1 if norm=='ln' else None))
        self.model.add_module("flatten", building_blocks._Flatten())
        self.model.add_module("linear", nn.Linear(final_nf, 1))
        self.model.add_module("sigmoid", nn.Sigmoid())
        
        self.alpha = 0.0
        self.grown = False
        
    def __intermediate_block(self, nch, double, norm='bn', fs=None):
        block = nn.Sequential()
        block.add_module("conv_1", building_blocks._ConvNormReLU(
            nch, nch, 3, 1, 1, 1, norm=norm, lrelu=True, fs=fs))
        block.add_module("conv_2", building_blocks._ConvNormReLU(
            nch, 2*nch if double else nch, 3, 1, 1, 1, norm=norm, lrelu=True, fs=fs))
        block.add_module("avg_pool", nn.AvgPool2d(2))
        return block
    
    def fuse(self, delta=0.1):
        self.alpha = min(1.0, self.alpha+delta)
        
    def grow(self):
        new_model = nn.Sequential()
        if self.grown:
            new_model.add_module(f"layer_{self.cur_res}x{self.cur_res}", self.top_layer)
        for name, module in self.model.named_children():
            new_model.add_module(name, module)
            new_model[-1].load_state_dict(module.state_dict())            
            # for param in new_model[-1].parameters():
            #     param.requires_grad = False

        self.model = None
        self.model = new_model
        
        self.prev_from_rgb = nn.Sequential()
        self.prev_from_rgb.add_module("avg_pool", nn.AvgPool2d(2))
        from_rgb = self.from_rgb
        # for p in from_rgb.parameters():
        #     p.requires_grad = False
        self.prev_from_rgb.add_module("from_rgb", from_rgb)
     
        self.cur_res *= 2
        self.cur_nf = self.cur_nf//2
        self.from_rgb = building_blocks._ConvNormReLU(
            self.input_channels, self.cur_nf, 1, 1, 0, 1,
            norm=self.norm, lrelu=True, fs=self.cur_res if self.norm=='ln' else None)
        
        self.top_layer = self.__intermediate_block(self.cur_nf, double=True, norm=self.norm,
                                                   fs=self.cur_res if self.norm=='ln' else None)
        
        self.grown = True
        self.alpha = 0.0
        
        
    def forward(self, x):
        if x.size()[2] != self.cur_res:
            raise Exception("Invalid input resolution.")
        if self.grown:
            top_layer_out = self.top_layer(self.from_rgb(x))
            old_branch_out = self.prev_from_rgb(x)
            return self.model(self.alpha*top_layer_out + (1-self.alpha)*old_branch_out)
        else:
            return self.model(self.from_rgb(x))
        
        
        
class Discriminator(nn.Module):
    def __init__(self, input_channels=3, n=64, input_size=64, max_channels=512, norm='bn'):
        super(Discriminator, self).__init__()
        self.ngpu = 1
        self.main = nn.Sequential()
        
        self.main.add_module('opening_conv', building_blocks._ConvNormReLU(input_channels, n, 4, 2, 1, 1, lrelu=True, norm='no'))
        # state size n x input_size/2 x input_size/2
        channels = n
        for i in range(int(math.log2(input_size/4)) - 1):
            self.main.add_module(f'conv_bn_relu_{i+1}', building_blocks._ConvNormReLU(
            min(channels, max_channels), min(channels*2, max_channels), 4,
            2, 1, 1, norm=norm, lrelu=True))
            channels *= 2
            
        self.main.add_module('final_conv', nn.Conv2d(min(channels, max_channels), 1, kernel_size=4, stride=1, padding=0, bias=True))
#         self.main.add_module('sigmoid', nn.Sigmoid())

    def forward(self, input):
        if isinstance(input.data, torch.cuda.FloatTensor) and self.ngpu > 1:
            output = nn.parallel.data_parallel(self.main, input, range(self.ngpu))
        else:
            output = self.main(input)

        return output.view(-1, 1)
    
    def weights_init(self):
        for m in self.modules():
            # if isinstance(m, nn.Conv2d):
            #     m.weight.data.normal_(0.0, 0.02)
            if isinstance(m, nn.BatchNorm2d):
                m.weight.data.normal_(1.0, 0.02)
                m.bias.data.fill_(0)