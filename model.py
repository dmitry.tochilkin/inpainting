import torch
from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F

from PIL import Image

import utils
import building_blocks
import unet_model
import unet_parts


class GrowableEncoderDecoderG(nn.Module):
    def __init__(self, in_channels=4, n=64, levels=6, pred_level=2, max_channels=512, norm='bn'):
        super(GrowableEncoderDecoderG, self).__init__()
        self.levels = levels
        self.pred_level = pred_level
        self.in_channels = in_channels
        self.n = n
        self.norm = norm
        
        self.n_channels_level = n*2**torch.Tensor(range(levels)).int()
        self.n_channels_level = torch.clamp(self.n_channels_level, 0, max_channels).tolist()
        self.kernel_sizes = (torch.ones(levels, dtype=torch.int32) * 3).tolist()
        self.kernel_sizes[0] = 7
        self.kernel_sizes[1] = 5
        self.kernel_sizes[2] = 5
        
        self.down_layers = nn.ModuleList()
        self.down_layers.append(unet_parts._Down(
                    in_channels, self.n_channels_level[0], kernel_size=self.kernel_sizes[0], norm=norm))
        for level in range(1, self.levels):
            self.down_layers.append(unet_parts._Down(
                self.n_channels_level[level-1], self.n_channels_level[level],
                kernel_size=self.kernel_sizes[level], norm=norm))
        self.non_local = building_blocks._NonLocalBlock2d(max_channels)
        
        self.up_layers = nn.ModuleList()
        for level in range(self.pred_level):
            self.up_layers.append(building_blocks._UpsampleBlock(
                self.n_channels_level[-level-1], self.n_channels_level[-level-2],
                kernel_size=3, norm=norm))
            
        self.to_rgb = nn.Conv2d(self.n_channels_level[-self.pred_level-1], 3, 1)
 
        self.alpha = 0.0
        self.grown = False
        
        
    def grow(self):
        if self.pred_level==self.levels:
            raise Exception("I don't wanna grow no more")
        
        if self.grown:
            self.up_layers.append(nn.Upsample(scale_factor=2, mode='bilinear'))
            self.up_layers.append(self.top_up_layer)
#             new_up_layer = building_blocks._UpsampleBlock(
#                 self.n_channels_level[-self.pred_level-1],
#                 self.n_channels_level[-self.pred_level],
#                 kernel_size=3, norm=self.norm)

        self.prev_to_rgb = self.to_rgb
    
        self.pred_level += 1
        self.top_up_layer = unet_parts._DoubleConv(
                self.n_channels_level[-self.pred_level],
                self.n//2 if self.pred_level==self.levels else self.n_channels_level[-self.pred_level-1],
                kernel_size=3, norm=self.norm)
        self.to_rgb = nn.Conv2d(self.n//2 if self.pred_level==self.levels else self.n_channels_level[-self.pred_level-1],
                                3, 1)
        
        self.grown = True
        self.aplha = 0.
                    
            
    def fuse(self, delta=0.1):
        self.alpha = min(1.0, self.alpha+delta)
    

    def forward(self, x):
        f = x
        for d in self.down_layers:
            f = d(f)
        f = self.non_local(f)
        for u in self.up_layers:
            f = u(f)
        if not self.grown:
            return self.to_rgb(f)
        else:
            f = F.upsample(f, scale_factor=2, mode='bilinear')
            old_branch_out = self.prev_to_rgb(f)
            top_layer_out = self.to_rgb(self.top_up_layer(f))
            return self.alpha*top_layer_out + (1-self.alpha)*old_branch_out



class EncoderDecoderG(nn.Module):
    def __init__(self, in_channels=4, n=64, levels=6, max_channels=512, norm='bn'):
        super(EncoderDecoderG, self).__init__()
        self.levels = levels
        self.in_channels = in_channels
        self.n_channels_level = n*2**torch.Tensor(range(levels)).int()
        self.n_channels_level = torch.clamp(self.n_channels_level, 0, max_channels).tolist()
        self.kernel_sizes = (torch.ones(levels, dtype=torch.int32) * 3).tolist()
        self.kernel_sizes[0] = 7
        self.kernel_sizes[1] = 5
        self.kernel_sizes[2] = 5
        
        self.down_layers = nn.ModuleList()
        self.down_layers.append(unet_parts._Down(
                    in_channels, self.n_channels_level[0], kernel_size=self.kernel_sizes[0], norm=norm))
        for level in range(1, self.levels):
            self.down_layers.append(unet_parts._Down(
                self.n_channels_level[level-1], self.n_channels_level[level],
                kernel_size=self.kernel_sizes[level], norm=norm))
        self.non_local = building_blocks._NonLocalBlock2d(max_channels)
        
        self.up_layers = nn.ModuleList()
        for level in reversed(range(1, self.levels)):
            self.up_layers.append(building_blocks._UpsampleBlock(
                self.n_channels_level[level], self.n_channels_level[level-1],
                kernel_size=3, norm=norm))
        self.up_layers.append(nn.Upsample(scale_factor=2))
        self.up_layers.append(building_blocks._ConvNormReLU(
                self.n_channels_level[0], self.n_channels_level[0], 3, 1, 1, 1, norm=norm))
        self.up_layers.append(building_blocks._ConvNormReLU(
                self.n_channels_level[0], 3, 3, 1, 1, 1, norm='no'))
    
    def forward(self, x):
        f = x
        for d in self.down_layers:
            f = d(f)
        f = self.non_local(f)
        for u in self.up_layers:
            f = u(f)
        return f


class LapIN(nn.Module):
    def __init__(self, growth_rate=32, block_config=(6, 12, 16), upsample_conv_k=3, # adjust number and sizes of blocks
                 num_init_features=64, bn=True, bn_size=4, drop_rate=0, input_mask=True):
        super(LapIN, self).__init__()
        
        self.block_config = block_config
        self.bn = bn
        self.avg_pool = nn.AvgPool2d(kernel_size=2, stride=2)
        self.in_channels = 4 if input_mask else 3
        self.opening_conv = nn.Conv2d(self.in_channels, num_init_features, kernel_size=5, stride=1, padding=2, bias=not bn)
#         if bn:
#             self.opening_bn = nn.BatchNorm2d(num_init_features)
        self.levels = len(block_config)
        self.upsample_conv_k = upsample_conv_k
        
        # Each denseblock
        num_features = num_init_features
        self.dense_blocks = nn.ModuleList()
        self.transitions = nn.ModuleList()
        self.atrous_modules = nn.ModuleList()
        self.after_atrous = nn.ModuleList()
        self.predict_convs = nn.ModuleList()
#         self.conv_upsampled = nn.ModuleList()
        self.upsample_convs = nn.ModuleList()
        for i, num_layers in enumerate(block_config):
            self.dense_blocks.append(building_blocks._DenseBlock(num_layers=num_layers, num_input_features=num_features,
                                bn_size=bn_size, growth_rate=growth_rate, drop_rate=drop_rate, bn=bn))
            num_features = num_features + num_layers * growth_rate
#             if i != len(block_config) - 1:
            self.transitions.append(building_blocks._Transition(num_input_features=num_features, num_output_features=num_features // 2, bn=bn))
            num_features = num_features // 2
            dilation_1 = 2**(self.levels - i)
            pyramid = [dilation_1, 2*dilation_1, 4*dilation_1]
            atrous_features = num_features
            if i == self.levels-1:
                self.atrous_modules.append(building_blocks._ASPPModule(num_features, atrous_features, pyramid, bn=bn))
            else:
                self.atrous_modules.append(building_blocks._ASPPModule(num_features+3, atrous_features, pyramid, bn=bn))
            
            self.after_atrous.append(nn.Conv2d(atrous_features * (len(pyramid) + 1), num_features, kernel_size=3, padding=1, bias=not bn))                
            self.predict_convs.append(nn.Conv2d(num_features, 3, kernel_size=3, padding=1, bias=not bn))
            
#             self.conv_upsampled.append(nn.Conv2d(3, 3, kernel_size=upsample_conv_k, padding=0, bias=False))
            up_conv = nn.ConvTranspose2d(3, 3, kernel_size=4, stride=2, padding=1, bias=not bn)
            up_conv.weight.data = utils.bilinear_kernel(4, 3, 3).float()
            self.upsample_convs.append(up_conv)
        
    def forward(self, x):
#         if self.bn:
#             features = F.relu(self.opening_bn(self.opening_conv(x)))
#         else:
        features = F.relu(self.opening_conv(x))
        feature_levels = []
        for i in range(self.levels):
            features = self.dense_blocks[i](features)
            features = self.transitions[i](features)
            if i != self.levels-1:
                feature_levels.append(features)
                features = self.avg_pool(features)
                
        predictions = []
        image_pred = self.predict_convs[2](self.after_atrous[2](self.atrous_modules[2](features)))
        predictions.append(image_pred)        
        for i in reversed(range(self.levels-1)):
            upsampled_pred = self.upsample_convs[i](image_pred)
            image_pred = self.predict_convs[i](self.after_atrous[i](self.atrous_modules[i](
                torch.cat((upsampled_pred, feature_levels[i]), dim=1)))) + upsampled_pred
            predictions.append(image_pred) 
        
        return tuple(predictions)
    
        
    def freeze_bn(self):
        for m in self.modules():
            if isinstance(m, nn.BatchNorm2d):
                m.eval()