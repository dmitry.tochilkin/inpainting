import torch
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
import numpy as np
from PIL import Image
import os
import cv2

class MaskGenerator():
    def __init__(self, img_size=256, max_strokes=5, min_width=20, max_width=60):
        self.img_size = img_size
        self.max_strokes = max_strokes
        self.min_width = min_width
        self.max_width = max_width
        
    def generate_mask(self):
        img = np.zeros((self.img_size, self.img_size), np.uint8)
        start_x=np.random.randint(0, self.img_size)
        start_y=np.random.randint(0, self.img_size)
        n_strokes=np.random.randint(1, self.max_strokes)
        for i in range(n_strokes):
            end_x=np.random.randint(0, self.img_size)
            end_y=np.random.randint(0, self.img_size)
            width = np.random.randint(self.min_width, self.max_width)
            img = cv2.line(img,(start_x,start_y),(end_x,end_y),(1),width)
            start_x,start_y=end_x,end_y
        return torch.Tensor(img).unsqueeze(0)
    
    def generate_mask_batch(self, batch_size):
        mask_list = [self.generate_mask() for i in range(batch_size)]
        return torch.stack(mask_list)

# class MaskGenerator():
#     def __init__(self, masks_dir='../data/masks', im_size=256, mask_frac=0.5):
#         self.masks = [os.path.join(masks_dir, m) for m in os.listdir(masks_dir)]
#         for m in self.masks:
#             if Image.open(m).size != (128, 128):
#                 self.masks.remove(m)
        
#         self.resize_mask = transforms.Resize((int(im_size*mask_frac), int(im_size*mask_frac)))
#         self.to_tensor = transforms.ToTensor()
#         self.mask_frac = mask_frac
#         self.im_size = im_size
            
#     def generate_mask(self):
#         mask = self.to_tensor(self.resize_mask(Image.open(self.masks[np.random.randint(0, len(self.masks))]))).float()
#         mask = mask / mask.max()
#         pos_h = np.random.randint(0, self.im_size - mask.size()[1])
#         pos_w = np.random.randint(0, self.im_size - mask.size()[2])
#         full_mask = torch.zeros((1, self.im_size, self.im_size))
#         full_mask[..., pos_h:pos_h+mask.size()[1], pos_w:pos_w+mask.size()[2]] = mask
#         mask_region_h = max(0, pos_h - int(self.im_size*self.mask_frac - mask.size()[1])//2)
#         mask_region_w = max(0, pos_w - int(self.im_size*self.mask_frac - mask.size()[2])//2)
#         return full_mask, mask_region_h, mask_region_w