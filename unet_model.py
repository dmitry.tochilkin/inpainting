import torch
import torch.nn as nn
import torch.nn.functional as F

import unet_parts
import utils
import building_blocks

class GrowableUNet(nn.Module):
    def __init__(self, in_channels, n=64, levels=6, pred_level=2):
        super(GrowableUNet, self).__init__()
        self.levels = levels
        self.pred_level = pred_level
        self.n = n
        self.in_channels = in_channels
        
        self.down_layers = nn.ModuleList()
        for level in range(self.levels):
            if level==0:
                self.down_layers.append(unet_parts._Down(
                    in_channels, n, kernel_size=7, norm='no'))
            elif level==1 or level==2:
                self.down_layers.append(unet_parts._Down(
                    n*2**(level-1), n*2**level, kernel_size=5))
            else:
                self.down_layers.append(unet_parts._Down(
                    min(n*8, n*2**(level-1)), min(n*8, n*2**level), kernel_size=3))
#                 print(f"Down_{level} {min(n*8, n*2**(level-1))}->{min(n*8, n*2**level)}")
        self.non_local = building_blocks._NonLocalBlock2d(min(n*8, n*2**level))
                
        self.up_layers = nn.ModuleList()
        for level in range(self.pred_level):
            self.up_layers.append(self._up_layer(level))

        self.to_rgb = self._to_rgb_layer()
            
        self.alpha = 0.0
        self.grown = False
        
    def _to_rgb_layer(self):
        if self.pred_level==self.levels:
            return nn.Conv2d(self.n, 3, 1)
        else:
            return nn.Conv2d(
                min(self.n*8, self.n*2**(self.levels-self.pred_level-1)), 3, 1)
        
    def _up_layer(self, level, upsample=True):
        if level == self.levels-1:
            return unet_parts._Up(self.n, self.in_channels, self.n, upsample=upsample)
        else:
            return unet_parts._Up(
                min(self.n*8, self.n*2**(self.levels-level-1)), min(self.n*8, self.n*2**(self.levels-level-2)),
                min(self.n*8, self.n*2**(self.levels-level-2)), upsample=upsample)
            
    def fuse(self, delta=0.1):
        self.alpha = min(1.0, self.alpha+delta)

    def grow(self):
        if self.pred_level==self.levels:
            raise Exception("I don't wanna grow no more")
   
        if self.grown:
            new_up_layer = self._up_layer(self.pred_level-1, upsample=True)
            new_up_layer.load_state_dict(self.top_layer.state_dict())
            self.up_layers.append(new_up_layer)

        self.prev_to_rgb = self.to_rgb

        # for child in self.children():
        #     for param in child.parameters():
        #         param.requires_grad = False

        self.top_layer = self._up_layer(self.pred_level, upsample=False)
        self.pred_level += 1
        self.to_rgb = self._to_rgb_layer()            
        
        self.grown=True
        self.alpha=0.0
        
    def freeze_bn(self):
        for m in self.modules():
            if isinstance(m, nn.BatchNorm2d):
                m.eval()
        
    def forward(self, x):
        down_outs = []
        down_outs.append(x)
        for down in self.down_layers:
            x = down(x)
            down_outs.append(x)
            
        # down_outs[-1] = self.non_local(down_outs[-1])
#             print(f"down_out_{x.size()}")
        
        f = self.up_layers[0](down_outs[-1], down_outs[-2])
        f = self.non_local(f)
#         print(f"up_out_{f.size()}")
        for level in range(1, len(self.up_layers)):
            f = self.up_layers[level](f, down_outs[-level-2])
#             print(f"up_out_{f.size()}")
        
        if not self.grown:
            return self.to_rgb(f)
        else:
            f = F.upsample(f, scale_factor=2, mode='bilinear')
            old_branch_out = self.prev_to_rgb(f)
            top_layer_out = self.to_rgb(self.top_layer(f, down_outs[-self.pred_level-1]))
            return self.alpha*top_layer_out + (1-self.alpha)*old_branch_out


class UNet(nn.Module):
    def __init__(self, n_channels, n=64):
        super(UNet, self).__init__()
        self.down1 = unet_parts._Down(n_channels, n, kernel_size=7, norm='no')
        self.down2 = unet_parts._Down(n, n*2, kernel_size=5)
        self.down3 = unet_parts._Down(n*2, n*4, kernel_size=5)
        self.down4 = unet_parts._Down(n*4, n*8, kernel_size=3)
        self.down5 = unet_parts._Down(n*8, n*8, kernel_size=3)
        self.down6 = unet_parts._Down(n*8, n*8, kernel_size=3)
        self.up1 = unet_parts._Up(n*8, n*8, n*8)
        self.up2 = unet_parts._Up(n*8, n*8, n*8)
        self.up3 = unet_parts._Up(n*8, n*4, n*4)
        self.up4 = unet_parts._Up(n*4, n*2, n*2)
        self.up5 = unet_parts._Up(n*2, n, n)
        self.up6 = unet_parts._Up(n, 4, n)
        self.pred_conv = building_blocks._ConvNormReLU(n, n//2, 3, 1, 1, 1)
        self.to_rgb = nn.Conv2d(n//2, 3, 1)
        
    def forward(self, x):
        x1 = self.down1(x)
        x2 = self.down2(x1)
        x3 = self.down3(x2)
        x4 = self.down4(x3)
        x5 = self.down5(x4)
        x6 = self.down6(x5)
        f = self.up1(x6, x5)
        f = self.up2(f, x4)
        f = self.up3(f, x3)
        f = self.up4(f, x2)
        f = self.up5(f, x1)
        f = self.up6(f, x)
        pred = self.to_rgb(self.pred_conv(f))
        return pred

# class UNet(nn.Module):
#     def __init__(self, n_channels, n=64, bn=False):
#         super(UNet, self).__init__()
#         # self.inc = unet_parts._InConv(n_channels, n)
#         self.down1 = unet_parts._Down(n_channels, n, kernel_size=7)
#         self.down2 = unet_parts._Down(n, n*2, kernel_size=5)
#         self.down3 = unet_parts._Down(n*2, n*4, kernel_size=5)
#         self.down4 = unet_parts._Down(n*4, n*8)
#         self.down5 = unet_parts._Down(n*8, n*8)
#         self.down6 = unet_parts._Down(n*8, n*8)
#         self.up1 = unet_parts._Up(n*8, n*8)
#         self.up2 = unet_parts._Up(n*8, n*4)
#         self.pred1 = nn.Conv2d(n*4, 3, kernel_size=3, padding=1, bias=True)
#         self.up3 = unet_parts._Up(n*4, n*2, additional_channels=0)
#         self.pred2 = nn.Conv2d(n*2, 3, kernel_size=3, padding=1, bias=True) 
#         self.up4 = unet_parts._Up(n*2, n, additional_channels=0)
#         self.pred3 = nn.Conv2d(n, 3, kernel_size=3, padding=1, bias=True)
#         self.up_pred1 = nn.ConvTranspose2d(3, 3, kernel_size=4, stride=2, padding=1, bias=not bn)
#         self.up_pred1.weight.data = utils.bilinear_kernel(4, 3, 3).float()
#         self.up_pred2 = nn.ConvTranspose2d(3, 3, kernel_size=4, stride=2, padding=1, bias=not bn)
#         self.up_pred2.weight.data = utils.bilinear_kernel(4, 3, 3).float()

#     def forward(self, x):
#         x1 = self.inc(x)
#         x2 = self.down1(x1)
#         x3 = self.down2(x2)
#         x4 = self.down3(x3)
#         x5 = self.down4(x4)
#         x = self.up1(x5, x4)
#         x = self.up2(x, x3)
#         preds = []
#         pred = self.pred1(x)
#         preds.append(pred)
#         upsampled_pred = self.up_pred1(pred)
#         x = self.up3(x, x2) #torch.cat([x2, upsampled_pred], dim=1))
#         pred = self.pred2(x) + upsampled_pred
#         preds.append(pred)
#         upsampled_pred = self.up_pred2(pred)
#         x = self.up4(x, x1) #torch.cat([x1, upsampled_pred], dim=1))
#         pred = self.pred3(x) + upsampled_pred
#         preds.append(pred)
#         return tuple(preds)