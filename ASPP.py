from collections import OrderedDict

import torch
import torch.nn as nn
import torch.nn.functional as F


class _ConvBatchNormReLU(nn.Sequential):
    def __init__(self, in_channels, out_channels, kernel_size, stride, padding, dilation, relu=True):
        super(_ConvBatchNormReLU, self).__init__()
        self.add_module(
            'conv',
            nn.Conv2d(
                in_channels=in_channels,
                out_channels=out_channels,
                kernel_size=kernel_size,
                stride=stride,
                padding=padding,
                dilation=dilation,
                bias=False,
            ),
        )
        self.add_module(
            'bn',
            nn.BatchNorm2d(
                num_features=out_channels,
                eps=1e-5,
                momentum=0.999,
                affine=True,
            ),
        )

        if relu:
            self.add_module('relu', nn.ReLU())

    def forward(self, x):
        return super(_ConvBatchNormReLU, self).forward(x)


class _ASPPModule(nn.Module):
    """Atrous Spatial Pyramid Pooling with image pool"""

    def __init__(self, in_channels, out_channels, pyramids):
        super(_ASPPModule, self).__init__()
        self.stages = nn.Module()
        self.stages.add_module(
            'c0',
            _ConvBatchNormReLU(in_channels, out_channels, 1, 1, 0, 1),
        )
        for i, (dilation, padding) in enumerate(zip(pyramids, pyramids)):
            self.stages.add_module(
                'c{}'.format(i + 1),
                _ConvBatchNormReLU(in_channels, out_channels, 3, 1, padding, dilation),
            )
        # Probably image pooling is not what we need in image inpainting task
        self.imagepool = nn.Sequential(
            OrderedDict([
                ('pool', nn.AdaptiveAvgPool2d(1)),
                ('conv', _ConvBatchNormReLU(in_channels, out_channels, 1, 1, 0, 1)),
            ])
        )

    def forward(self, x):
        h = self.imagepool(x)
        h = [F.upsample(h, size=x.shape[2:], mode='bilinear')]
        for stage in self.stages.children():
            h += [stage(x)]
        h = torch.cat(h, dim=1)
        return h
