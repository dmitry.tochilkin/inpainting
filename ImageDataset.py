import torch
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
import numpy as np
from PIL import Image
import os

class ImageDataset(Dataset):
    def __init__(self, root_dirs, im_size=256, mean_color=[ 0.47,  0.447,  0.407], normalize=True):
        self.filenames = []
        for root_dir in root_dirs:
            for f in os.listdir(root_dir):
                filename = os.path.join(root_dir, f)
                try:
                    img = Image.open(filename)
#                     shape = np.array(img).shape
#                     r,g,b = img.getpixel((0,0))
                    if min(img.size[0], img.size[1]) >= im_size: # and len(shape) == 3 and shape[2] == 3: # and not r==g==b:
                        self.filenames.append(filename)
                    else:
                        os.remove(filename)
                except Exception:
                    pass
            
        self.preprocess = transforms.Compose([
            transforms.RandomResizedCrop(size=im_size, ratio=(1,1)),
            transforms.RandomHorizontalFlip()
        ])
        self.to_tensor = transforms.ToTensor()
        self.im_size = im_size
        self.normalize = normalize
        if mean_color is None:
            self.get_mean_color()
        else:
            self.mean_color = torch.Tensor(mean_color).unsqueeze(1).unsqueeze(2)
        
        if self.normalize:
            self.background = torch.zeros(3,im_size,im_size)
        else:
            self.background = torch.ones(3,im_size,im_size) * self.mean_color

    def __len__(self):
        return len(self.filenames)
    
    def get_mean_color(self):
        rgb_mean = np.zeros(3)
        for f in self.filenames:
            try:
                img = self.to_tensor(Image.open(f))
                if img.size()[0] == 3:
                    for c in range(3):
                        rgb_mean[c] += img[c].mean()
                else:
                    self.filenames.remove(f)
            except IOError:
                pass
        self.mean_color = torch.Tensor(rgb_mean / len(self.filenames)).unsqueeze(1).unsqueeze(2)

    def __getitem__(self, idx):
        img = Image.open(self.filenames[idx])
        augmented_img = self.to_tensor(self.preprocess(img))
        if self.normalize:
            return augmented_img - self.mean_color
        else:
            return augmented_img
        
#         a = dict()
#         a['gt'] = augmented_img #* 2 - 1
#         augmented_img = augmented_img * (1-mask) + self.background * mask
# #         for i in range(len(self.mean_color)):
# #             augmented_img[i] = augmented_img[i].masked_fill_(self.mask, self.mean_color[i])
#         a['distorted'] = augmented_img #* 2 - 1
#         a['mask'] = mask
#         a['mask_region'] = mask_start
#         return a