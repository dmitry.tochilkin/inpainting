import torch
import torch.nn as nn
import torch.nn.functional as F

import utils
import building_blocks


class _DoubleConv(nn.Module):
    '''(conv => BN => ReLU) * 2'''
    def __init__(self, in_ch, out_ch, kernel_size=3, norm='bn'):
        super(_DoubleConv, self).__init__()
        self.conv = nn.Sequential()
        self.conv.add_module('conv_bn_relu_1', building_blocks._ConvNormReLU(
            in_ch, out_ch, kernel_size, 1, 1, 1, norm=norm))
        self.conv.add_module('conv_bn_relu_2', building_blocks._ConvNormReLU(
            out_ch, out_ch, kernel_size, 1, 1, 1, norm=norm))

    def forward(self, x):
        x = self.conv(x)
        return x


class _InConv(nn.Module):
    def __init__(self, in_ch, out_ch):
        super(_InConv, self).__init__()
        self.conv = _DoubleConv(in_ch, out_ch)

    def forward(self, x):
        x = self.conv(x)
        return x


class _Down(nn.Module):
    def __init__(self, in_ch, out_ch, norm='bn', strided=True, kernel_size=3):
        super(_Down, self).__init__()
        self.mpconv = nn.Sequential()
        if strided:
            self.mpconv.add_module('conv_bn_relu', building_blocks._ConvNormReLU(
                in_ch, out_ch, kernel_size, 2, kernel_size/2, 1, norm='bn', part_conv=True))
            # self.mpconv.add_module('conv_bn_relu2', building_blocks._ConvNormReLU(
            #     out_ch, out_ch, 3, 1, 1, 1, norm='bn', part_conv=True))
        else:
            self.mpconv.add_module('max_pool', nn.MaxPool2d(2))
            self.mpconv.add_module('double_conv', _DoubleConv(in_ch, out_ch, bn=bn))

    def forward(self, x):
        x = self.mpconv(x)
        return x


class _Up(nn.Module):
    def __init__(self, in_ch1, in_ch2, out_ch, upsample=True, norm='bn'):
        super(_Up, self).__init__()
        if upsample:
            self.up = nn.Sequential()
            self.up.add_module('up', nn.Upsample(scale_factor=2))
            self.up.add_module('conv_in1', nn.Conv2d(in_ch1, in_ch1, 3, padding=1))
        else:
            self.up = nn.Sequential()
            self.up.add_module('conv_in1', nn.Conv2d(in_ch1, in_ch1, 3, padding=1))
            # self.up = nn.ConvTranspose2d(in_ch1, in_ch1, 4, stride=2, padding=1, bias=not bn)
            # self.up.weight.data = utils.bilinear_kernel(4, in_ch1, in_ch1).float()

        self.conv = building_blocks._ConvNormReLU(
            in_ch1+in_ch2, out_ch, 3, 1, 1, 1, norm=norm, lrelu=True, part_conv=True)

    def forward(self, x1, x2):
        x1 = self.up(x1)
        x = torch.cat([x1, x2], dim=1)
        x = self.conv(x)
        return x