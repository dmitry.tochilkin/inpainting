import torch
import torch.nn as nn
import numpy as np
from torch.autograd import Variable, grad


def get_hole_surrounding_mask(mask, size):
    hole_ind1 = mask.sum(1).nonzero()
    hole_ind2 = mask.sum(0).nonzero()
    hole_width = int(hole_ind1[-1] - hole_ind1[0] + 1)
    hole_height = int(hole_ind2[-1] - hole_ind2[0] + 1)
    if max(hole_width, hole_height) > size:
        raise ValueError("Hole is too big! I can't handle it :(")
    res = torch.zeros_like(mask)
    sur_x_start = int(hole_ind1[0]) - (size-hole_width)//2
    sur_y_start = int(hole_ind2[0]) - (size-hole_height)//2
    res[sur_y_start:sur_y_start+size, sur_x_start:sur_x_start+size] = 1
    return res

def get_n_params(model):
    pp=0
    for p in list(model.parameters()):
        nn=1
        for s in list(p.size()):
            nn = nn*s
        pp += nn
    return pp

def get_pyramid(image_batch, levels, downsampler):
    pyramid = []
    downsampler = nn.AvgPool2d(2)
    cur_level = image_batch[:]
    for i in range(levels):
        pyramid.append(cur_level)
        if i != levels-1:
            cur_level = downsampler(cur_level)
    return pyramid

        
def bilinear_kernel(k, inputs, outputs):
    r = k // 2
    if k%2==1:
        center=r
    else:
        center=r+0.5

    c=np.arange(1,k+1)
    kernel_1d = np.ones((1,k)) - np.abs(c-center) / r
    kernel_2d = kernel_1d.T * kernel_1d
    return torch.from_numpy(np.tile(kernel_2d, (inputs, outputs, 1, 1))) 

def calc_gradient_penalty(netD, real_data, fake_data, batch_size, device, mask=None):
    alpha = torch.rand(batch_size, 1, 1, 1).cuda(device=device)

    interpolates = alpha * real_data + ((1 - alpha) * fake_data)
    interpolates = Variable(interpolates, requires_grad=True)
    disc_interpolates = netD(interpolates)

    gradients = grad(outputs=disc_interpolates, inputs=interpolates,
                     grad_outputs=torch.ones(disc_interpolates.size()).cuda(device=device),
                     create_graph=True, retain_graph=True, only_inputs=True)[0]
    
    # if mask is not None:
    #     gradients = gradients * mask

    gradients = gradients.view(gradients.size(0), -1)
    gradient_penalty = ((gradients.norm(2, dim=1) - 1) ** 2).mean()
    return gradient_penalty