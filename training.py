import torch
from torch.autograd import Variable, grad
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

from torch.utils.data import DataLoader
from torchvision import transforms, utils

import ImageDataset

from PIL import Image
import numpy as np
import math
import os
from datetime import date

import utils
import model
import Logger
import Discriminator
import building_blocks
import mask_generation
import ssim
import tv
import unet_model

device=1
image_size = 256
bs_train = 16
bs_val = 4
train_dataset = ImageDataset.ImageDataset(["../data/cityscapes/train"], im_size=image_size,
                                          mean_color=[0.376, 0.434, 0.45], normalize=True)
data_loader = DataLoader(train_dataset, batch_size=bs_train, shuffle=True, pin_memory=True, num_workers=3)
val_dataset = ImageDataset.ImageDataset(["../data/cityscapes/test"], im_size=image_size,
                                        mean_color=[0.376, 0.434, 0.45], normalize=True)
val_loader = DataLoader(train_dataset, batch_size=bs_val, shuffle=True, pin_memory=True, num_workers=3)
mask_gen = mask_generation.MaskGenerator(img_size=image_size)

parallel = False
# G_net = model.LapIN(block_config=block_config, bn=False).cuda()
# G_net = unet_model.UNet(4, 48).cuda(device=device)
G_net = model.EncoderDecoderG(in_channels=4, n=48, norm='bn').cuda(device=device)
D_net = Discriminator.Discriminator(input_channels=4, input_size=256, norm='bn').cuda(device=device)

rec_criterion = nn.L1Loss(size_average=False, reduce=False).cuda(device=device)
# ssim_criterion = ssim.SSIMLoss(window_size=5)
tv_criterion = tv.TVLoss()
l1_error = nn.L1Loss().cuda(device=device)
l2_error = nn.MSELoss().cuda(device=device)
# adv_criterion = nn.BCELoss().cuda(device=device)

critic = 'lsgan'

adv_loss_weight = 0.7
gp_weight = 1.
tv_loss_weight = 0.001
rec_valid_weight = 0.

# G_net.load_state_dict(torch.load('model/unet_in/G_difmasks_moreiters_bnD_gatedconv_growable_scheduler_myunet_cs_l1_lsgan_1906_bs16_lrg1e-04_lrd1e-04_advw3e-02_gpw1e+00_2'), strict=False)
# D_net.load_state_dict(torch.load('model/unet_in/D_difmasks_moreiters_bnD_gatedconv_growable_scheduler_myunet_cs_l1_lsgan_1906_bs16_lrg1e-04_lrd1e-04_advw3e-02_gpw1e+00_2'), strict=False)

lrg = 1e-4
optimizerG = optim.Adam(G_net.parameters(), lr=lrg, betas=(0.5, 0.999))
schedulerG = optim.lr_scheduler.ReduceLROnPlateau(optimizerG, 'min', factor=0.4, patience=20)

lrd = 1e-3
# filter(lambda p: p.requires_grad, D_net.parameters())
D_optimizer = optim.Adam(filter(lambda p: p.requires_grad, D_net.parameters()), lr=lrd, betas=(0.5, 0.999))
schedulerD = optim.lr_scheduler.ReduceLROnPlateau(D_optimizer, 'min', factor=0.4, patience=20)

session = "encdec_moreiters_bnD_gatedconv_scheduler_myunet_cs_l1_lsgan_{}_bs{}_lrg{:.0e}_lrd{:.0e}_advw{:.0e}_gpw{:.0e}".format(
    date.today().strftime('%d%m'), bs_train, lrg, lrd, adv_loss_weight, gp_weight)
print('***** STARTING ' + session)
logger = Logger.Logger('./logs_lap_in', session)
log_step=0
val_iters = 4
for epoch in range(0, 10001):
    for i, input_batch in enumerate(data_loader):
        gt = input_batch.cuda(device=device)
        batch_size = gt.size(0)
        if batch_size != bs_train:
            continue
        mask = mask_gen.generate_mask() #mask_gen.generate_mask_batch(batch_size)
        # imp_mask = Variable(mask.clone()).cuda(device=device)
        mask[mask>0]=1
        mask = mask.cuda(device=device)
        distorted = gt * (1-mask) + train_dataset.background.cuda(device=device) * mask
        distorted = torch.cat((distorted, mask.expand(batch_size, -1, -1, -1)), dim=1) #mask.expand(batch_size, -1, -1, -1)
        G_output = G_net(Variable(distorted))

#         real_labels = Variable(torch.ones(batch_size).cuda(async=parallel))
#         fake_labels = Variable(torch.zeros(batch_size).cuda(async=parallel))
#         lowres_gt = x2_downsampler(x2_downsampler(Variable(gt)))
#         lowres_mask = x2_downsampler(x2_downsampler(Variable(mask)))
        cur_gt = Variable(gt)
        cur_mask = Variable(mask)
        cur_mask = cur_mask.expand(batch_size, -1, -1, -1)
        neg_mask = 1-cur_mask
        
        D_net.zero_grad()
        real_data=torch.cat((cur_gt, cur_mask), dim=1)
        D_real_output = D_net(real_data)
        
        pred = cur_mask*G_output + neg_mask*cur_gt
        fake_data = torch.cat((pred, cur_mask), dim=1).detach()
        D_fake_output = D_net(fake_data)
        
        if critic == 'wgan-gp':
            errD_real = -D_real_output.mean()
            errD_fake = D_fake_output.mean()
            gradient_penalty = utils.calc_gradient_penalty(
                D_net, real_data.data, fake_data.data, batch_size, device=device, mask=cur_mask)
            errD = errD_fake + errD_real + gp_weight * gradient_penalty            
        elif critic == 'lsgan':
            errD_real = ((D_real_output - 1)**2).mean()
            errD_fake = (D_fake_output**2).mean()
            errD = 0.5 * (errD_real + errD_fake)
        else:
            raise Exception('Unsupported critic.')
        
        errD.backward()
        D_optimizer.step()

        if i%3 == 0:
            G_net.zero_grad()
            # rec_loss_hole = rec_criterion(G_output * cur_mask, cur_gt * cur_mask)
            rec_loss_hole = (rec_criterion(G_output * cur_mask, cur_gt * cur_mask).view(batch_size, -1).sum(1) / cur_mask.view(batch_size,-1).sum(1)).mean()
            # rec_loss_valid = rec_criterion(G_output * neg_mask, cur_gt * neg_mask)
            rec_loss_valid = (rec_criterion(G_output * neg_mask, cur_gt * neg_mask).view(batch_size, -1).sum(1)/neg_mask.view(batch_size,-1).sum(1)).mean()
            tv_loss = tv_criterion(pred)

            GD_fake_output = D_net(torch.cat((pred, cur_mask), dim=1))
            if critic == 'wgan-gp':
                err_GD = -GD_fake_output.mean()
            elif critic == 'lsgan':
                err_GD = 0.5 * torch.mean((GD_fake_output - 1)**2)

            total_rec = rec_loss_hole + rec_valid_weight*rec_loss_valid
            errG = total_rec + adv_loss_weight*err_GD
            errG.backward()
            optimizerG.step()
        
        
        if i % 3000 == 0 and epoch % 5 == 0:
            log_step +=1
            print("D loss: {:.4f}, G_rec loss: {:.4f}, G_D loss: {:.4f}, TV: {:.4f}".format(
                errD.item(), total_rec.item(), err_GD.item(), tv_loss.item()))

            print("D real: {:.4f}, D fake: {:.4f}".format(errD_real, errD_fake))
            #============ TensorBoard logging ============#
            # (1) Log the scalar values
            logger.scalar_summary("errD_real", errD_real.item(), log_step)
            logger.scalar_summary("errD_fake", errD_fake.item(), log_step)
            logger.scalar_summary("errD_total", errD.item(), log_step)
            logger.scalar_summary("errGD", err_GD.item(), log_step)
            logger.scalar_summary("total G", errG.item(), log_step)
            logger.scalar_summary("total train rec", total_rec.item(), log_step)
            logger.scalar_summary("total train tv", tv_loss.item(), log_step)
            
            del gt, G_output
            torch.cuda.empty_cache()
            
            l1_err = l2_err = ssim_err = tv_err = 0
            for val_iter, val_batch in enumerate(val_loader):
                if val_iter >= val_iters:
                    break
                
                val_gt = val_batch.cuda(device=device)
                val_batch_size = val_gt.size(0)
                mask = mask_gen.generate_mask()
                # imp_mask = Variable(mask.clone()).cuda(device=device)
                mask[mask>0]=1
                mask = mask.cuda(device=device)
                distorted = val_gt * (1-mask) + train_dataset.background.cuda(device=device) * mask
                distorted = torch.cat((distorted, mask.expand(val_batch_size, -1, -1, -1)), dim=1)
                G_output = G_net(Variable(distorted))
                
                ind = np.random.randint(0, val_batch_size)
                current_gt = Variable(val_gt)
                current_mask = Variable(mask)
                prediction = current_mask*G_output + (1-current_mask)*current_gt
                l1_err += l1_error(prediction, current_gt).item()
                l2_err += l2_error(prediction, current_gt).item()
                ssim_err += ssim.ssim(prediction, current_gt).item()
                tv_err += tv_criterion(prediction).item()
                for ind in range(val_batch_size):
                    sample_to_log = prediction[ind].data.cpu().numpy()
                    logger.image_summary('val_image_{}'.format(log_step), [sample_to_log], epoch+1)

            logger.scalar_summary("l1_val", l1_err/val_iters, log_step)
            logger.scalar_summary("l2_val", l2_err/val_iters, log_step)
            logger.scalar_summary("ssim_val", ssim_err/val_iters, log_step)
            logger.scalar_summary("tv_val", tv_err/val_iters, log_step)
                
            del val_gt, G_output
            torch.cuda.empty_cache()

            torch.save(D_net.state_dict(), 'model/unet_in/D_'+session)
            torch.save(G_net.state_dict(), 'model/unet_in/G_'+session)
    
    schedulerG.step(errG.item())
    schedulerD.step(errG.item())